﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace FastDictionary
{
	// This is an awful Benchmark because string.GetHashCode takes longer time than anything else
	// [HardwareCounters(HardwareCounter.BranchMispredictions, HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired)]
	public class Benchmark_GetValue
	{
		Dictionary<string, int> DefaultDictionary;
		FastDictionary<string, int> FastDictionary;

		const int StringCount = 100_000;
		const int StringLengthMin = 10;
		const int StringLengthMax = 200;
		const int GetValueCount = 1_000_000;

		string[] Items = new string[StringCount];
		int[] CheckIndices = new int[GetValueCount];
		
		public Benchmark_GetValue()
		{
			DefaultDictionary = new Dictionary<string, int>();
			FastDictionary = new FastDictionary<string, int>();
			
			var alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var random = new Random();

			// Create random strings with random length
			for(int i = 0; i < StringCount; i++)
			{
				var length = random.Next(StringLengthMin, StringLengthMax);
				var chars = new char[length];

				for(int j = 0; j < length; j++)
				{
					chars[j] = alphanumeric[random.Next(alphanumeric.Length)];
				}

				Items[i] = new string(chars);
			}
			
			// Insert all strings to both dictionaries
			for(int i = 0; i < StringCount; i++)
			{
				var str = Items[i];
				DefaultDictionary.Add(str, i);
				FastDictionary.Add(str, i);
			}
			
			// Initialize random check indices here (so that random.Next() doesn't interfere with benchmark)
			for(int i = 0; i < GetValueCount; i++)
			{
				CheckIndices[i] = random.Next(StringCount);
			}
		}

		[Benchmark]
		public void GetValueFastDictionary()
		{
			for(int i = 0; i < GetValueCount; i++)
			{
				var index = CheckIndices[i];
				var str = Items[index];
				var value = FastDictionary[str];
			}
		}

		[Benchmark]
		public void GetValueDefaultDictionary()
		{
			for(int i = 0; i < GetValueCount; i++)
			{
				var index = CheckIndices[i];
				var str = Items[index];
				var value = DefaultDictionary[str];
			}
		}
	}
}