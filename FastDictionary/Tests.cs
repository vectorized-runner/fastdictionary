﻿using System;
using System.Diagnostics.Tracing;
using NUnit.Framework;

namespace FastDictionary
{
	public class Tests
	{
		FastDictionary<string, int> Dictionary;
		
		[SetUp]
		public void SetUp()
		{
			Dictionary = new FastDictionary<string, int>(4);
		}

		[Test]
		public void ToStringIsSame()
		{
			var first = "bbb";
			var hash1 = first.GetHashCode();
			var hash2 = first.ToString().GetHashCode();
			
			Assert.AreEqual(hash1, hash2);
		}

		[Test]
		public void Binary_0()
		{
			var b1 = 0b_0001;
			var b2 = 0b_0010;
			
			Assert.AreEqual(0b_0011, b1 | b2);
		}
		
		[Test]
		public void Binary_1()
		{
			var b1 = 0b_0001;
			var b2 = 0b_0010;
			
			Assert.AreEqual(0b_0000, b1 & b2);
		}

		[Test]
		public void Binary_2()
		{			
			var b1 = 0b_1111;
			var b2 = 0b_001;
			
			Assert.AreEqual(0b_0001, b1 & b2);
		}
		
		[Test]
		public void Binary_3()
		{			
			var b1 = 0b_1011;
			var b2 = 0b_001;
			
			Assert.AreEqual(0b_1011, b1 | b2);
		}

		[Test]
		public void Binary_4()
		{
			var b0 = 0b_1111;
			var clear = 0x7FFFFFFF;
			
			Assert.AreEqual(0b_1111, b0 & clear);
		}
		
		[Test]
		public void Binary_5()
		{
			var b0 = 0b_1000;
			var clear = 0x7FFFFFFF;
			
			Assert.AreEqual(0b_1000, b0 & clear);
		}

		[Test]
		public void Binary_6()
		{
			// This is the bit hack, clears the sign bit
			var num = 0b_1111_1111_1111_1111_1111_1111_1111_1111;
			var clear = 0x7FFFFFFF;
			var result = 0b_0111_1111_1111_1111_1111_1111_1111_1111;
			
			Assert.AreEqual(result, num & clear);
		}
		
		[Test]
		public void InitialCapacityShouldBeGreaterThanZero()
		{
			Assert.Throws<Exception>(() => { Dictionary = new FastDictionary<string, int>(0); });
			Assert.Throws<Exception>(() => { Dictionary = new FastDictionary<string, int>(-1); });
			Assert.Throws<Exception>(() => { Dictionary = new FastDictionary<string, int>(-100); });
		}

		[Test]
		public void CantDeleteNonExistingElement()
		{
			Dictionary.Add("aaa", 3);
			Assert.IsFalse(Dictionary.Remove("bbb"));
		}

		[Test]
		public void CountBeforeAddOnce()
		{
			Assert.AreEqual(0, Dictionary.Count);
		}

		[Test]
		public void CountAfterAddOnce()
		{
			Dictionary.Add(string.Empty, 0);
			Assert.AreEqual(1, Dictionary.Count);
		}

		[Test]
		public void ContainsAfterAdd()
		{
			Dictionary.Add("aaa", 0);
			Assert.IsTrue(Dictionary.ContainsKey("aaa"));
		}

		[Test]
		public void AddOnceThenFind()
		{
			Dictionary.Add("aaa", 3);
			Assert.AreEqual(3, Dictionary["aaa"]);
		}

		[Test]
		public void AddTwiceThenFind()
		{
			Dictionary.Add("aaa", 3);
			Assert.AreEqual(3, Dictionary["aaa"]);
			
			Dictionary.Add("bbb", 4);
			Assert.AreEqual(4, Dictionary["bbb"]);

			// This is critical
			Assert.AreEqual(3, Dictionary["aaa"]);
		}

		[Test]
		public void AddTwiceReverseOrder()
		{
			Dictionary.Add("bbb", 4);
			Dictionary.Add("aaa", 3);

			Assert.AreEqual(3, Dictionary["aaa"]);
			Assert.AreEqual(4, Dictionary["bbb"]);
		}

		[Test]
		public void CheckContainsKey()
		{
			Assert.IsFalse(Dictionary.ContainsKey("aaa"));

			Dictionary.Add("aaa", 5);

			Assert.IsTrue(Dictionary.ContainsKey("aaa"));

			Dictionary.Remove("aaa");

			Assert.IsFalse(Dictionary.ContainsKey("aaa"));
		}
		
		[Test]
		public void AddTwoThenFind()
		{
			Dictionary.Add("aaa", 1);
			Dictionary.Add("bbb", 22);
			
			Assert.DoesNotThrow(() => { var item = Dictionary["aaa"]; });
			Assert.DoesNotThrow(() => { var item = Dictionary["bbb"]; });
		}

		[Test]
		public void AddThreeThenFind()
		{
			Dictionary.Add("aaa", 1);
			Dictionary.Add("bbb", 22);
			Dictionary.Add("ccc", 333);
			
			Assert.DoesNotThrow(() => { var item = Dictionary["aaa"]; });
			Assert.DoesNotThrow(() => { var item = Dictionary["bbb"]; });
			Assert.DoesNotThrow(() => { var item = Dictionary["ccc"]; });
		}

		[Test]
		public void DoesNotThrowExceptionOnFindAfterInsert()
		{
			Dictionary.Add("aaa", 1);
			Assert.DoesNotThrow(() =>
			{
				var myValue = Dictionary["aaa"];
			});
		}

		[Test]
		public void ThrowExceptionFindAfterDeletionSingle()
		{
			Dictionary.Add("aaa", 1);
			Dictionary.Remove("aaa");
			
			Assert.Throws<Exception>(() =>
			{
				var myValue = Dictionary["aaa"];
			});
		}
		
		[Test]
		public void ThrowsExceptionWhenFindAfterDeletion()
		{
			Dictionary.Add("aaa", 1);
			Dictionary.Add("bbb", 22);
			Dictionary.Add("ccc", 333);
			Dictionary.Add("ddd", 4444);
			Dictionary.Add("eee", 55555);
			Dictionary.Add("fff", 66666);
			Dictionary.Add("ggg", 777777);
			Dictionary.Add("hhh", 8888888);
			Dictionary.Add("jjj", 99999999);
			Dictionary.Add("kkk", 100000000);
			
			Dictionary.Remove("aaa");
			Dictionary.Remove("bbb");
			Dictionary.Remove("ccc");
			Dictionary.Remove("ddd");
			Dictionary.Remove("eee");
			Dictionary.Remove("fff");
			Dictionary.Remove("ggg");
			Dictionary.Remove("hhh");
			Dictionary.Remove("jjj");
			Dictionary.Remove("kkk");

			Assert.Throws<Exception>(() => { var item = Dictionary["aaa"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["bbb"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["ccc"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["ddd"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["eee"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["fff"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["ggg"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["hhh"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["jjj"]; });
			Assert.Throws<Exception>(() => { var item = Dictionary["kkk"]; });
		}

		[Test]
		public void CheckValuesAfterAddition()
		{
			Dictionary.Add("aaa", 1);
			Dictionary.Add("bbb", 22);
			Dictionary.Add("ccc", 333);
			Dictionary.Add("ddd", 4444);
			Dictionary.Add("eee", 55555);
			Dictionary.Add("fff", 66666);
			Dictionary.Add("ggg", 777777);
			Dictionary.Add("hhh", 8888888);
			Dictionary.Add("jjj", 99999999);
			Dictionary.Add("kkk", 100000000);

			Assert.AreEqual(1, Dictionary["aaa"]);
			Assert.AreEqual(22, Dictionary["bbb"]);
			Assert.AreEqual(333, Dictionary["ccc"]);
			Assert.AreEqual(4444, Dictionary["ddd"]);
			Assert.AreEqual(55555, Dictionary["eee"]);
			Assert.AreEqual(66666, Dictionary["fff"]);
			Assert.AreEqual(777777, Dictionary["ggg"]);
			Assert.AreEqual(8888888, Dictionary["hhh"]);
			Assert.AreEqual(99999999, Dictionary["jjj"]);
			Assert.AreEqual(100000000, Dictionary["kkk"]);
		}

		[Test]
		public void SetItemWorks()
		{
			Dictionary.Add("aaa", 3);
			Dictionary["aaa"] = 5;

			Assert.AreEqual(5, Dictionary["aaa"]);
		}

		[Test]
		public void TestCount()
		{
			Assert.AreEqual(0, Dictionary.Count);

			Dictionary.Add("aaa", 1);
			Assert.AreEqual(1, Dictionary.Count);

			Dictionary.Add("ccc", 333);
			Dictionary.Add("ddd", 4444);
			Dictionary.Add("eee", 55555);
			Assert.AreEqual(4, Dictionary.Count);

			Dictionary.Remove("ccc");
			Dictionary.Remove("fff");
			Assert.AreEqual(3, Dictionary.Count);
		}

		[Test]
		public void NothingContainsAfterClear()
		{
			Dictionary.Add("aaa", 1);
			Dictionary.Add("bbb", 22);
			Dictionary.Add("ccc", 333);
			Dictionary.Add("ddd", 4444);
			Dictionary.Add("eee", 55555);
			Dictionary.Add("fff", 66666);
			Dictionary.Add("ggg", 777777);
			Dictionary.Add("hhh", 8888888);
			Dictionary.Add("jjj", 99999999);
			Dictionary.Add("kkk", 100000000);

			Dictionary.Clear();

			Assert.IsFalse(Dictionary.ContainsKey("aaa"));
			Assert.IsFalse(Dictionary.ContainsKey("bbb"));
			Assert.IsFalse(Dictionary.ContainsKey("ccc"));
			Assert.IsFalse(Dictionary.ContainsKey("ddd"));
			Assert.IsFalse(Dictionary.ContainsKey("eee"));
			Assert.IsFalse(Dictionary.ContainsKey("fff"));
			Assert.IsFalse(Dictionary.ContainsKey("ggg"));
			Assert.IsFalse(Dictionary.ContainsKey("hhh"));
			Assert.IsFalse(Dictionary.ContainsKey("jjj"));
			Assert.IsFalse(Dictionary.ContainsKey("kkk"));
			
			Assert.AreEqual(0, Dictionary.Count);
		}

		[Test]
		public void DoesntContainAfterClear()
		{
			Dictionary.Add("aaa", 3);
			Dictionary.Remove("aaa");

			Assert.Throws<Exception>(() =>
			{
				var myValue = Dictionary["aaa"];
			});
		}
	}
}