﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace FastDictionary
{

	[ShortRunJob]
	// [HardwareCounters(HardwareCounter.BranchMispredictions, HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired)]
	public class Benchmark_GetValue_Struct
	{
		public readonly struct MyStruct : IEquatable<MyStruct>
		{
			public readonly int A;
			public readonly int B;
			public readonly int C;
			public readonly int D;

			public MyStruct(int a, int b, int c, int d)
			{
				A = a;
				B = b;
				C = c;
				D = d;
			}

			public bool Equals(MyStruct other)
			{
				return A == other.A && B == other.B && C == other.C && D == other.D;
			}

			public override bool Equals(object obj)
			{
				return obj is MyStruct other && Equals(other);
			}

			public override int GetHashCode()
			{
				unchecked
				{
					var hash = 17;
					hash = hash * 31 + A;
					hash = hash * 31 + B;
					hash = hash * 31 + C;
					hash = hash * 31 + D;
					return hash;
				}
			}

			public static bool operator ==(MyStruct left, MyStruct right)
			{
				return left.Equals(right);
			}

			public static bool operator !=(MyStruct left, MyStruct right)
			{
				return !left.Equals(right);
			}
		}
		
		Dictionary<MyStruct, int> DefaultDictionary;
		FastDictionary<MyStruct, int> FastDictionary;

		const int InsertCount = 100_000;
		const int RemoveCount = 10_000;
		const int GetValueCount = 1_000_000;

		MyStruct[] Items = new MyStruct[InsertCount];
		int[] CheckIndices = new int[GetValueCount];
		
		public Benchmark_GetValue_Struct()
		{
			DefaultDictionary = new Dictionary<MyStruct, int>();
			FastDictionary = new FastDictionary<MyStruct, int>();
			
			var alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var random = new Random();

			// Create random strings with random length
			for(int i = 0; i < InsertCount; i++)
			{
				Items[i] = new MyStruct(random.Next(), random.Next(), random.Next(), random.Next());
			}
			
			// Insert all strings to both dictionaries
			for(int i = 0; i < InsertCount; i++)
			{
				var item = Items[i];
				DefaultDictionary.Add(item, i);
				FastDictionary.Add(item, i);
			}

			for(int i = 0; i < RemoveCount; i++)
			{
				var item = Items[random.Next(InsertCount)];
				DefaultDictionary.Remove(item);
				FastDictionary.Remove(item);
			}
			
			// Initialize random check indices here (so that random.Next() doesn't interfere with benchmark)
			for(int i = 0; i < GetValueCount; i++)
			{
				CheckIndices[i] = random.Next(InsertCount);
			}
		}

		[Benchmark]
		public void GetValueFastDictionary()
		{
			for(int i = 0; i < GetValueCount; i++)
			{
				var index = CheckIndices[i];
				var str = Items[index];
				var exists = FastDictionary.TryGetValue(str, out _);
			}
		}

		[Benchmark]
		public void GetValueDefaultDictionary()
		{
			for(int i = 0; i < GetValueCount; i++)
			{
				var index = CheckIndices[i];
				var str = Items[index];
				var exists = DefaultDictionary.TryGetValue(str, out _);
			}
		}
	}
}